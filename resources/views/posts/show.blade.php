@extends('layouts.app')

@section('content')
    <div style="margin-top:2%;">
        <a href="http://localhost/Laravel/Laravel/public/posts" class="btn btn-primary">Go back</a>
        <br/>
        <br/>
        <h1>{{$post->title}}</h1>
        <br/>
        <br/>
        <img style="width:100%" src="\Laravel\Laravel\public\storage\cover_images\{{$post->cover_image}}">
        <br/>
        <br/>
        <div>
            {!!$post->body!!}
        </div>
        <hr>
        <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
        <hr>
    </div>
    <div style="margin-bottom:2%;">
        @if(!Auth::guest())
            <a href="http://localhost/Laravel/Laravel/public/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>

            {!!Form::open(['action'=>['PostsController@destroy', $post->id], 'method'=>'POST', 'style'=>'float:right;'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class'=>'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    </div>
@endsection